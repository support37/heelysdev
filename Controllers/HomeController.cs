﻿using ShopifySharp;
using ShopifySharp.Filters;
using ShopifySharp.Lists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace HeelysDev.Controllers
{
    public class HomeController : Controller
    {
        private async Task<IEnumerable<Order>> GetOrder()
        {

            var myShopifyUrl = "https://heelysdev.myshopify.com/";
            var privateAppPassword = "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";
            var service = new OrderService(myShopifyUrl, privateAppPassword);
            OrderListFilter filter = new OrderListFilter()
            {
                FinancialStatus = "refunded",
                Status = "any"
            };
            var ordersasync = await service.ListAsync(filter);
            var orders = ordersasync.Items;
            return orders;
        }
     
        public ActionResult Index()
        {
            var orders = GetOrder();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}